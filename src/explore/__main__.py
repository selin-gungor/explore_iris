import argparse
import logging
from .train_test import train_and_test


def main(testing_set_size: int = 20) -> None:
	"""
	Main function
	:param testing_set_size:
	:return:
	"""
	logging.basicConfig(level=logging.DEBUG)
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument("--testing-set-size", "-tss", type=int, required=False,
	                    help="Testing set size")
	
	args = parser.parse_args()
	
	if args.testing_set_size is not None:
		logging.info(f"--testing-set-size is provided, getting testing set size '{str(args.testing_set_size)}'")
		testing_set_size = args.testing_set_size
		train_and_test(testing_set_size)
	else:
		logging.info(f"Running with the default testing set size '{testing_set_size}'")
		train_and_test(testing_set_size)
