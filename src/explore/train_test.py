import numpy as np
import logging
from sklearn import datasets, tree, metrics


def load_dataset():
	return datasets.load_iris(), datasets.load_digits()


def train_and_test(testing_set_size: int):
	iris, digits = load_dataset()
	
	iris_data = iris.data
	iris_target = iris.target
	
	logging.debug(len(iris_data))
	logging.debug(type(iris_data))
	
	# Split the dataset randomly
	iris_test_ids = np.random.permutation(len(iris_data))
	
	# Leave the last 20 entries for testing
	iris_train_set_first = iris_data[iris_test_ids[:-testing_set_size]]
	iris_test_set_first = iris_data[iris_test_ids[-testing_set_size:]]
	
	iris_train_set_second = iris_target[iris_test_ids[:-testing_set_size]]
	iris_test_set_second = iris_target[iris_test_ids[-testing_set_size:]]
	
	iris_classifier = tree.DecisionTreeClassifier()
	iris_classifier.fit(iris_train_set_first, iris_train_set_second)
	
	predicted_flowers = iris_classifier.predict(iris_test_set_first)
	
	accuracy_score = metrics.accuracy_score(predicted_flowers, iris_test_set_second) * 100
	
	logging.info("Predicted flower species")
	logging.info(predicted_flowers)
	logging.info("Actual flower species")
	logging.info(iris_test_set_second)
	logging.info("Accuaracy score: " + str(accuracy_score))
