from src.explore.train_test import load_dataset


def test_load_dataset():
	data, digits = load_dataset()
	assert data is not None
	assert digits is not None
